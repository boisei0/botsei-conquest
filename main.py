import sys
import argparse
from parser import Parser

__author__ = 'boisei0'


class CommandlineParser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(description=u'Conquest bot')
        self.parser.add_argument('--log', dest="log", metavar="FILE", help=u'Absolute path to the log to feed as input')
        self.parser.add_argument('--debug', dest="debug", default=False, action="store_true", help=u'Enable debug mode')

    def parse_args(self):
        return self.parser.parse_args()


def main():
    args = CommandlineParser().parse_args()
    parser = Parser(debug=args.debug)

    if not args.log:
        while True:
            line = sys.stdin.readline().strip()
            if len(line) == 0:
                continue
            parser.parse(line.split(' '))
    else:
        with open(args.log) as f:
            log = f.readlines()
            for line in log:
                line = line.strip()
                if len(line) == 0:
                    continue
                parser.parse(line.split(' '))


if __name__ == '__main__':
    main()
