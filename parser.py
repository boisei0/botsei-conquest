class FlushObject(object):
    def __init__(self, f):
        self.f = f

    def write(self, x):
        self.f.write(x)
        self.f.flush()

import sys
import models

sys.stdout = FlushObject(sys.stdout)

__author__ = 'boisei0'


class Parser:
    def __init__(self, debug=False):
        self.debug_mode = debug

        self._bot_name = 'player1'
        self._opponent_name = 'player2'
        self._neutral_name = 'neutral'
        self._starting_armies = 5
        self._super_regions = dict()
        self._regions = dict()
        self._neighbours = dict()
        self._map = dict()

        self.cmd_handler = Handler(self._bot_name, self._opponent_name, self._neutral_name, self._starting_armies,
                                   debug=self.debug_mode)

    def parse(self, command_parts):
        if command_parts[0] == 'setup_map':
            if command_parts[1] == 'super_regions':
                self._parse_super_regions(command_parts[2:])
            elif command_parts[1] == 'regions':
                self._parse_regions(command_parts[2:])
            elif command_parts[1] == 'neighbors':
                self._parse_neighbors(command_parts[2:])
        elif command_parts[0] == 'pick_starting_regions':
            self.cmd_handler.pick_starting_regions(command_parts[2:])
        elif command_parts[0] == 'settings':
            if command_parts[1] == 'your_bot':
                self._bot_name = command_parts[2]
                self.cmd_handler.update_bot_name(self._bot_name)
            elif command_parts[1] == 'opponent_bot':
                self._opponent_name = command_parts[2]
                self.cmd_handler.update_opponent_name(self._opponent_name)
            elif command_parts[1] == 'starting_armies':
                self._starting_armies = int(command_parts[2])
                self.cmd_handler.update_starting_armies(self._starting_armies)
        elif command_parts[0] == 'update_map':
            self._parse_map(command_parts[1:])
        elif command_parts[0] == 'opponent_moves':
            self._parse_opponent_moves(command_parts[1:])
        elif command_parts[0] == 'go':
            if command_parts[1] == 'place_armies':
                self.cmd_handler.place_armies(self._map)
            elif command_parts[1] == 'attack/transfer':
                self.cmd_handler.attack_transfer_armies(self._map)

    def _parse_super_regions(self, data):
        n_super_regions = int(len(data) / 2)
        for n in range(0, n_super_regions):
            self._super_regions[data[n * 2]] = int(data[n * 2 + 1])  # [superregionID] = armyBonus
        if self.debug_mode:
            print('DEBUG: {}'.format(self._super_regions))

    def _parse_regions(self, data):
        n_regions = int(len(data) / 2)
        for n in range(0, n_regions):
            self._regions[data[n * 2]] = data[n * 2 + 1]  # [regionID] = superregionID
        if self.debug_mode:
            print('DEBUG (self._regions): {}'.format(self._regions))

    def _parse_neighbors(self, data):
        if self.debug_mode:
            print('DEBUG: {}'.format(';'.join(data)))
        items = int(len(data) / 2)
        for n in range(0, items):
            if ',' in data[n * 2]:
                continue

            region_id = data[n * 2]
            neighbours = self._get_reverse_neighbours(region_id)

            try:
                if not ',' in data[n * 2 + 1]:
                    neighbours.append(data[n * 2 + 1])
                else:
                    neighbours.extend(data[n * 2 + 1].split(','))
            except IndexError:
                # n is last item
                pass

            self._neighbours[region_id] = neighbours

        for n in range(1, 43):
            region_id = str(n)
            if self._neighbours.get(region_id) is None:
                self._neighbours[region_id] = self._get_reverse_neighbours(region_id)

        if self.debug_mode:
            print('DEBUG (neighbours): {}'.format(self._neighbours))

    def _get_reverse_neighbours(self, region_id):
        neighbours = []
        for k, v in self._neighbours.iteritems():
            if region_id in v:
                neighbours.append(k)
        return neighbours

    def _parse_map(self, data):
        n_visible_regions = int(len(data) / 3)
        for n in range(0, n_visible_regions):
            region_id = data[n * 3]
            owner = data[n * 3 + 1]
            armies = int(data[n * 3 + 2])
            self._map[region_id] = models.Region(region_id, self._regions[region_id], self._neighbours[region_id],
                                                 owner, armies)

    @staticmethod
    def _parse_opponent_moves(data):
        n_moves = int(len(data) / 5)
        for n in range(0, n_moves):
            bot = data[n * 5]
            action = data[n * 5 + 1]
            attack_region = data[n * 5 + 2]
            defend_region = data[n * 5 + 3]
            attacking_armies = data[n * 5 + 4]
            # perhaps I need to do something with this data


class Handler:
    def __init__(self, bot_name, opponent_name, neutral_name, starting_armies, debug=False):
        self.debug_mode = debug

        self._bot_name = bot_name
        self._opponent_name = opponent_name
        self._neutral_name = neutral_name
        self._starting_armies = starting_armies

    def update_bot_name(self, bot_name):
        self._bot_name = bot_name

    def update_opponent_name(self, opponent_name):
        self._opponent_name = opponent_name

    def update_neutral_name(self, neutral_name):
        self._neutral_name = neutral_name

    def update_starting_armies(self, starting_armies):
        self._starting_armies = starting_armies

    @staticmethod
    def pick_starting_regions(ids):
        print(' '.join([ids[i] for i in range(6)]))

    def place_armies(self, conquest_map):
        output = ''
        armies = self._starting_armies

        regions_owned = self._get_bot_regions(conquest_map)
        neutral_regions = self._get_neutral_regions(conquest_map)
        enemy_regions = self._get_opponent_regions(conquest_map)
        for regionID, region in conquest_map.iteritems():
            if regionID in regions_owned:
                available_armies = region.armies
                for neighbour_id in region.neighbours:
                    try:
                        needed_armies = conquest_map[neighbour_id].armies * 2
                        if neighbour_id in neutral_regions or neighbour_id in enemy_regions:
                            if available_armies < needed_armies:
                                if armies > 0:
                                    if armies > needed_armies:
                                        output += '{} place_armies {} {},'.format(self._bot_name, regionID,
                                                                                  needed_armies)
                                        armies -= needed_armies
                                    else:
                                        output += '{} place_armies {} {},'.format(self._bot_name, regionID, armies)
                                        armies = 0
                        elif neighbour_id in regions_owned:
                            # does not need armies
                            pass
                    except KeyError as ex:  # why do you fail?
                        if self.debug_mode:
                            print('ERROR: Handler.place_armies; KeyError: {}'.format(ex))

        if armies > 0 and 'place_armies' in output:
            # give all leftover armies to first region in output
            lucky_region_id = output.split(' ')[2]
            output += '{} place_armies {} {}'.format(self._bot_name, lucky_region_id, armies)
            armies = 0

        if self.debug_mode:
            print('DEBUG: Armies left at the end of the turn: {}'.format(armies))

        if output == '':
            output = 'No moves'
        print(output)

    def attack_transfer_armies(self, conquest_map):
        output = ''

        regions_owned = self._get_bot_regions(conquest_map)
        neutral_regions = self._get_neutral_regions(conquest_map)
        enemy_regions = self._get_opponent_regions(conquest_map)
        for region_id, region in conquest_map.iteritems():
            if region_id in regions_owned:
                available_armies = region.armies
                if available_armies > 1 and self._are_all_neighbours_owned(conquest_map, region_id):
                    first_grade_neighbours_with_nfn = set()
                    second_grade_neighbours_with_nfn = set()
                    for neighbour_id in region.neighbours:
                        if not self._are_all_neighbours_owned(conquest_map, neighbour_id):
                            first_grade_neighbours_with_nfn.add(neighbour_id)
                        else:
                            for second_grade_neighbour_id in conquest_map[neighbour_id].neighbours:
                                if not self._are_all_neighbours_owned(conquest_map, second_grade_neighbour_id):
                                    second_grade_neighbours_with_nfn.add(second_grade_neighbour_id)

                    # Remove duplicates from list
                    first_grade_neighbours_with_nfn = list(set(first_grade_neighbours_with_nfn))
                    second_grade_neighbours_with_nfn = list(set(second_grade_neighbours_with_nfn))

                    if first_grade_neighbours_with_nfn == list() and second_grade_neighbours_with_nfn != list():
                        # TODO: cleanup
                        if self.debug_mode:
                            print('DEBUG: {} ({}) has 2nd grade neighbour needing armies; neighbour(s): {} {}'.format(
                                region_id, region.armies, first_grade_neighbours_with_nfn,
                                second_grade_neighbours_with_nfn
                            ))
                        n_second_grade_needing_armies = len(second_grade_neighbours_with_nfn)
                        if n_second_grade_needing_armies != 0:  # this check should not be necessary...
                            transfer_armies = int((available_armies - 1) / n_second_grade_needing_armies)
                            if transfer_armies == 0:
                                # take first from the list and move all armies in that direction
                                neighbour_needing_armies = ''
                                for neighbour_id in region.neighbours:
                                    if second_grade_neighbours_with_nfn[0] in conquest_map[neighbour_id].neighbours:
                                        neighbour_needing_armies = neighbour_id
                                        break

                                output += '{} attack/transfer {} {} {},'.format(self._bot_name, region_id,
                                                                                neighbour_needing_armies,
                                                                                available_armies - 1)
                                available_armies = 1
                            else:
                                neighbours_needing_armies = set()
                                for neighbour_id in region.neighbours:
                                    for second_grade_neighbour_id in second_grade_neighbours_with_nfn:
                                        if second_grade_neighbour_id in conquest_map[neighbour_id].neighbours:
                                            neighbours_needing_armies.add(neighbour_id)

                                for neighbour_id in neighbours_needing_armies:
                                    output += '{} attack/transfer {} {} {},'.format(self._bot_name, region_id,
                                                                                    neighbour_id, transfer_armies)
                                    available_armies -= transfer_armies

                    elif first_grade_neighbours_with_nfn is list():
                        # TODO: 2nd grade also friendly, move armies to 3rd grade maybe?
                        pass

                    else:
                        n_neighbours_needing_armies = len(first_grade_neighbours_with_nfn)
                        if n_neighbours_needing_armies != 0:  # this check should not be necessary...
                            transfer_armies = int((available_armies - 1) / n_neighbours_needing_armies)
                            if transfer_armies == 0:
                                # take first from the list and give them all the available armies
                                output += '{} attack/transfer {} {} {},'.format(self._bot_name, region_id,
                                                                                first_grade_neighbours_with_nfn[0],
                                                                                available_armies - 1)
                                available_armies = 1
                            else:
                                for neighbour_id in first_grade_neighbours_with_nfn:
                                    output += '{} attack/transfer {} {} {},'.format(self._bot_name, region_id,
                                                                                    neighbour_id, transfer_armies)
                                    available_armies -= transfer_armies

                for neighbour_id in region.neighbours:
                    try:
                        needed_armies = conquest_map[neighbour_id].armies * 2
                        if (neighbour_id in neutral_regions or neighbour_id in enemy_regions) and available_armies > needed_armies:
                            output += '{} attack/transfer {} {} {},'.format(self._bot_name, region_id, neighbour_id,
                                                                            needed_armies)
                            available_armies -= needed_armies
                    except KeyError as ex:  # why do you fail?
                        if self.debug_mode:
                            print('ERROR: Handler.attack_transfer_armies; KeyError: {}'.format(ex))

        if output == '':
            output = 'No moves'
        print(output)

    def _get_bot_regions(self, conquest_map):
        return [regionID for regionID, region_obj in conquest_map.iteritems() if region_obj.owner == self._bot_name]

    def _get_neutral_regions(self, conquest_map):
        return [regionID for regionID, region_obj in conquest_map.iteritems() if region_obj.owner == self._neutral_name]

    def _get_opponent_regions(self, conquest_map):
        return [regionID for regionID, region_obj in conquest_map.iteritems()
                if region_obj.owner == self._opponent_name]

    def _are_all_neighbours_owned(self, conquest_map, region_id):
        try:
            for neighbour_id in conquest_map[region_id].neighbours:
                if conquest_map[neighbour_id].owner != self._bot_name:
                    return False
            return True
        except KeyError as ex:
            if self.debug_mode:
                print('ERROR: Handler._are_all_neighbours_owned; Possible reason: Fog of war; KeyError: {}'.format(ex))
            return False